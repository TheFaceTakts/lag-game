#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>

#include "sendDefines.h"

extern int errno;
int err;


int W;
int H;

const int MAX_MESSAGE_LENGTH = 256 * 257;
const int MAX_COMMAND_LENGTH = 50;
char bigBuffer[MAX_MESSAGE_LENGTH];
char command[MAX_COMMAND_LENGTH];
pthread_mutex_t mutex;

int mainSocket;

void handleError(char *message);
void checkError(char *message);
bool sendAll(int socket, void *buffer, int length);
bool sendPackageStyle(int socket, int length, int additionalInfo, void *buffer);
bool receiveAll(int socket, void *buffer, int length);
bool receivePackageStyle(int socket, int *length, int *additionalInfo, void *buffer);
void dieFunction(int socket);
void registerHandler(int signal, void (*handler)(int));
void mutexLock(void);
void mutexUnlock(void);
void *receiver(void *args);

void handleError(char *message)
{
    perror(message);
    exit(1);
}

void checkError(char *message)
{
    if (errno)
        handleError(message);
    if (err)
    {
        fprintf(stderr, "pthread_%s: (%d)%s\n", message, err, strerror(err));
        pthread_exit(0);
    }
}

void dieFunction(int socket)
{
    printf("Server died\n");
    close(socket);
    exit(0);
}

bool sendAll(int socket, void *buffer, int length)
{
    if (length == 0)
        return true;
    int total = 0;
    int current = 0;
    while (total < length && (current = write(socket, buffer + total, length - total) ) )
    {
        if (current == -1)
        {
            if (errno == ECONNRESET || errno == EPIPE)
            {
                dieFunction(socket);
                return true;
            }
            handleError("write");
        }
        total += current;
    }
    if (current == 0) //means client has disconnected;
    {
        dieFunction(socket);
        return false;
    }
    return true;
}

bool sendPackageStyle(int socket, int length, int additionalInfo, void *buffer)
{
    if (sendAll(socket, &length, sizeof(int)))
        if (sendAll(socket, &additionalInfo, sizeof(int)))
            if (sendAll(socket, buffer, length))
                return true;
    return false;
}

bool receiveAll(int socket, void *buffer, int length)
{
    if (length == 0)
        return true;
    int current = 0;
    int total = 0;
    while (total < length && (current = read(socket, buffer + total, length - total)))
    {
        if (errno == ECONNRESET)
        {
            dieFunction(socket);
            return false;
        }
        checkError("current");
        total += current;
    }
    if (current == 0)
    {
        dieFunction(socket);
        return false;
    }
    return true;
}

bool receivePackageStyle(int socket, int *length, int *additionalInfo, void *buffer)
{
    if (receiveAll(socket, length, sizeof(int)))
    {
        if (receiveAll(socket, additionalInfo, sizeof(int)))
            if (receiveAll(socket, buffer, *length))
                return true;
    }
    return false;
}

void registerHandler(int signal, void (*handler)(int))
{
    struct sigaction action;
    action.sa_handler = handler;
    sigemptyset(&action.sa_mask);
    checkError("sigemptyset");
    action.sa_flags = 0;
    sigaction (signal, &action, NULL);
    checkError("sigaction");
}

void handleBreak(int signal)
{
    sendPackageStyle(mainSocket, 0, SEND_DISCONNECT, NULL);
    close(mainSocket);
    exit(0);
}

void mutexLock()
{
    err = pthread_mutex_lock(&mutex);
    checkError("mutex_lock");
}

void mutexUnlock()
{
    err = pthread_mutex_unlock(&mutex);
    checkError("mutex_unlock");
}

void *receiver(void *args)
{
    int length, additionalInfo;
    while (1)
    {
        receivePackageStyle(mainSocket, &length, &additionalInfo, bigBuffer);
        mutexLock();
        if (
            additionalInfo == SEND_DESCRIPTION ||
            additionalInfo == SEND_LIST_OF_MAPS ||
            additionalInfo == SEND_LIST_OF_GAMES ||
            additionalInfo == SEND_LIST_OF_PLAYERS_IN_GAME ||
            additionalInfo == SEND_ALARM ||
            additionalInfo == SEND_ALREADY_IN_GAME ||
            additionalInfo == SEND_GAME_OVERFLOW ||
            additionalInfo == SEND_NOT_ALLOWED
        )   
        {
            
            printf("%s\n", bigBuffer);
            
        }
        else if (additionalInfo == SEND_MAP)
        {
            printf("\033c");
            W = ((int *) bigBuffer)[0];
            H = ((int *) bigBuffer)[1];
            for (int j = H - 1; j >= 0; --j)
            {
                for (int i = 0; i < W; ++i)
                {
                    printf("%c", *(bigBuffer + 2 * sizeof(int) + H * i + j)); // acess j i
                }
                printf("\n");
            }
        }
        else if (true)
        {
            printf("WTF? %d\n", additionalInfo);
        }
        mutexUnlock();
    }
    return NULL;
}

int main(int argc, char **argv)
{
    err = pthread_mutex_init(&mutex, NULL);
    checkError("mutex_init");
    
    registerHandler(SIGINT, handleBreak);
    if (argc < 4)
    {
        printf("Usage name host port\n");
        return 0;
    }
    mainSocket = socket(AF_INET, SOCK_STREAM, 0);
    checkError("socket");
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[3]));
    addr.sin_addr.s_addr = inet_addr(argv[2]);
    
    connect(mainSocket, (struct sockaddr *)&addr, sizeof(addr));
    checkError("conect");
    
    sendPackageStyle(mainSocket, strlen(argv[1]) + 1, SEND_NAME, argv[1]);
    
    pthread_t thread;
    err = pthread_create(&thread, NULL, receiver, NULL);
    checkError("create");
    
    while (true)
    {
        fgets(bigBuffer, MAX_MESSAGE_LENGTH, stdin);
        sscanf(bigBuffer, "%s", command);
        int commandLength = strlen(command);
        if (!strcmp(command, "list"))
        {
            sendPackageStyle(mainSocket, 0, SEND_GET_LIST, NULL);
        }
        else if (!strcmp(command, "maps"))
        {
            sendPackageStyle(mainSocket, 0, SEND_GET_MAPS, NULL);
        }
        else if (!strcmp(command, "new"))
        {
            char argument[MAX_COMMAND_LENGTH];
            sscanf(bigBuffer + commandLength, "%s", argument);
            sendPackageStyle(mainSocket, strlen(argument) + 1, SEND_NEW, argument);
        }
        else if (!strcmp(command, "connect"))
        {
            int gameNumber;
            char arg[MAX_COMMAND_LENGTH];
            sscanf(bigBuffer + commandLength, "%d %s", &gameNumber, arg);
            int mode = 0;
            if (!strcmp(arg, "spectator"))
            {
                mode = CLIENT_MODE_SPECTATOR;
            }
            else if (!strcmp(arg, "gamer"))
            {
                mode = CLIENT_MODE_GAMER;
            }
            else
            {
                mutexLock();
                printf("Incorrect mode\n");
                mutexUnlock();
                continue;
            }
            ((int *)arg)[0] = gameNumber;
            ((int *)arg)[1] = mode;
            sendPackageStyle(mainSocket, 2 * sizeof(int), SEND_CONNECT, arg);
        }
        else if (!strcmp(command, "players"))
        {
            int gameNumber;
            sscanf(bigBuffer + commandLength, "%d", &gameNumber);
            sendPackageStyle(mainSocket, sizeof(int), SEND_GET_PLAYERS, &gameNumber);
        }
        else if (!strcmp(command, "start"))
        {
            sendPackageStyle(mainSocket, 0, SEND_START, NULL);
        }
        else if (!strcmp(bigBuffer, "\n"))
        {
            sendPackageStyle(mainSocket, 0, SEND_MOVE_FORWARD, NULL);
        }
        else if (!strcmp(command, "w"))
        {
            sendPackageStyle(mainSocket, 0, SEND_MOVE_UP, NULL);
        }
        else if (!strcmp(command, "d"))
        {
            sendPackageStyle(mainSocket, 0, SEND_MOVE_RIGHT, NULL);
        }
        else if (!strcmp(command, "s"))
        {
            sendPackageStyle(mainSocket, 0, SEND_MOVE_DOWN, NULL);
        }
        else if (!strcmp(command, "a"))
        {
            sendPackageStyle(mainSocket, 0, SEND_MOVE_LEFT, NULL);
        }
        else if (!strcmp(command, "v"))
        {
            sendPackageStyle(mainSocket, 0, SEND_MOVE_SHOOT, NULL);
        }
        else if (!strcmp(command, "log"))
        {
            int gameNumber;
            sscanf(bigBuffer + commandLength, "%d", &gameNumber);
            sendPackageStyle(mainSocket, sizeof(int), SEND_WATCH_LOG, &gameNumber);
        }
        else
        {
            for (int i = 0; i < 3; ++i)
                printf("%d ", (int)bigBuffer[i]);
            printf("\n");
            mutexLock();
            printf("Incorrect command\n");
            mutexUnlock();
            continue;
        }
    }
    
    err = pthread_mutex_destroy(&mutex);
    checkError("mutex_destroy");
    
    return 0;
}
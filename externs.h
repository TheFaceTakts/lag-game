#include "serverHeader.h"

extern int errno;
extern const char MAPS_PATH[];
extern const char STARTED[];
extern const char NOT_STARTED[];

extern int mainSocket;
extern struct epoll_event mainEv;
extern struct epoll_event mainEvents[MAX_CLIENTS];
extern int mainEpollFileDescriptor;
extern const int epollTimeOut;

extern int gameNumber;
extern const char logDir[];


extern char introductionMessage[];

extern Game games[MAX_GAMES];
extern int freeGameIds[MAX_GAMES];
extern int lastFreeGame;

Client clients[MAX_CLIENTS];
extern int freeClientIds[MAX_CLIENTS];
extern int lastFreeClient;

extern char listOfMaps[MAX_SUM_MAPS_NAMES_LEN];
extern int listOfMapsLength;

extern Viewer viewers[MAX_CLIENTS];
extern int freeViewersIds[MAX_CLIENTS];
extern int lastFreeViewer;

extern const char *const gLockFilePath;

extern FILE *LOG_FILE;

#include "externs.h"

int max(int a, int b)
{
    return (a > b ? a : b);
}

int min(int a, int b)
{
    return (a < b ? a : b);
}

bool belongs(int A, int B, int C)
{
    
    return (min(A, B) <= C && C <= max(A, B));
}

bool onLineIntersection(int y1, int y2, int y3, int y4)
{
    return (belongs(y1, y2, y3) || belongs(y1, y2, y4) ||
        belongs(y3, y4, y1) || belongs(y3, y4, y2));

}

int sign(int x)
{
    if (x > 0)
        return 1;
    if (x == 0)
        return 0;
    return -1;
}

bool vectorCheck(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
{
    return sign((x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)) != sign((x2 - x1) * (y4 - y1) - (y2 - y1) * (x4 - x1));
}

bool intersectsSegments(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
{
    if (x1 == x2 && x3 == x4)
    {
        if (x1 != x3)
            return false;
        return onLineIntersection(y1, y2, y3, y4);
    }
    
    if (y1 == y2 && y3 == y4)
    {
        if (y1 != y3)
            return false;
        return onLineIntersection(x1, x2, x3, x4);
    }
    
    return vectorCheck(x1, y1, x2, y2, x3, y3, x4, y4) && vectorCheck(x3, y3, x4, y4, x1, y1, x2, y2);
}

bool intersects(int x1, int y1, int x2, int y2, int x3, int y3)
{
    return intersectsSegments(x1, y1, x2, y2, x3 - 1, y3 - 1, x3 - 1, y3 + 1) ||
        intersectsSegments(x1, y1, x2, y2, x3 - 1, y3 + 1, x3 + 1, y3 + 1) ||
        intersectsSegments(x1, y1, x2, y2, x3 + 1, y3 + 1, x3 + 1, y3 - 1) ||
        intersectsSegments(x1, y1, x2, y2, x3 + 1, y3 - 1, x3 - 1, y3 - 1);
}

bool weakIntersects(int x1, int y1, int x2, int y2, int x3, int y3)
{
    x1 = 2 * x1 + 1;
    y1 = 2 * y1 + 1;
    x2 = 2 * x2 + 1;
    y2 = 2 * y2 + 1;
    x3 = 2 * x3 + 1;
    y3 = 2 * y3 + 1;
    return (intersects(x1 - 1, y1 - 1, x2, y2, x3, y3) && 
            intersects(x1 - 1, y1 + 1, x2, y2, x3, y3) && 
            intersects(x1 + 1, y1 - 1, x2, y2, x3, y3) && 
            intersects(x1 + 1, y1 + 1, x2, y2, x3, y3));
}

bool visibleBlocks(int gameId, int x, int y, int x0, int y0)
{
    for (int i = 0; i < games[gameId].W; ++i)
        for (int j = 0; j < games[gameId].H; ++j)
            if (*accessToMap(&games[gameId], i, j) == '#')
            {
//                 printf("%d %d %d %d\n", 2 * i + 1, 2 * j + 1, x, y);
                if (intersects(x0, y0, x, y, 2 * i + 1, 2 * j + 1) && (x != 2 * i + 1 || y != 2 * j + 1))
                    return false;
            }
    return true;
}

bool visibleBlocksWeak(int gameId, int x, int y, int x0, int y0)
{
    return 
    visibleBlocks(gameId, 2 * x + 1, 2 * y + 1, 2 * x0 + 1, 2 * y0 + 1) ||
    visibleBlocks(gameId, 2 * x + 1, 2 * y + 1, 2 * x0 + 1 + 1, 2 * y0 + 1 + 1) ||
    visibleBlocks(gameId, 2 * x + 1, 2 * y + 1, 2 * x0 + 1 + 1, 2 * y0 + 1 - 1) ||
    visibleBlocks(gameId, 2 * x + 1, 2 * y + 1, 2 * x0 + 1 - 1, 2 * y0 + 1 + 1) ||
    visibleBlocks(gameId, 2 * x + 1, 2 * y + 1, 2 * x0 + 1 - 1, 2 * y0 + 1 - 1);
    
}

void gameEpollInit(int gameId)
{
    games[gameId].epfd = epoll_create(MAX_CLIENTS_IN_GAME);
    checkError("epoll_create");
    
    for (int i = 0; i < games[gameId].numberOfClients; ++i)
    {
        games[gameId].ev.events = EPOLLIN | EPOLLET | EPOLLRDHUP;
        games[gameId].ev.data.fd = clients[games[gameId].clients[i]].socket;
        
        epoll_ctl(mainEpollFileDescriptor, EPOLL_CTL_DEL, clients[games[gameId].clients[i]].socket, NULL);
        checkBrokenPipe(clients[games[gameId].clients[i]].socket);
        checkError("epoll_ctl del");
        
        epoll_ctl(games[gameId].epfd, EPOLL_CTL_ADD, clients[games[gameId].clients[i]].socket, &games[gameId].ev);
        checkBrokenPipe(clients[games[gameId].clients[i]].socket);
        checkError("epoll_ctl add");
        printf("thread epoll registered %d\n", clients[games[gameId].clients[i]].socket);
    }
}

int sq(int x)
{
    return x * x;
}

bool visibleUndirected(int gameId, int id, int x, int y)
{
    if (sq(x - clients[id].x[LAST]) + 4 * sq(y - clients[id].y[LAST]) >= sq(games[gameId].R))
    {
        return false;
    }
    return true;
}

bool visibleDirected(int gameId, int id, int x, int y)
{
    switch (getDirection(clients[id].dx[LAST], clients[id].dy[LAST]))
    {
        case 'v':
            if (y > clients[id].y[LAST])
                return false;
        break; case '>':
            if (x < clients[id].x[LAST])
                return false;
        break; case '<':
            if (x > clients[id].x[LAST])
                return false;
        break; case '^':
            if (y < clients[id].y[LAST])
                return false;
        break;
    }
    return visibleUndirected(gameId, id, x, y);
}


char getDirection(int dx, int dy)
{
    if (dx == 0 && dy == 1)
        return '^';
    else if (dx == 0 && dy == -1)
        return 'v';
    else if (dx == 1 && dy == 0)
        return '>';
    else if (dx == -1 && dy == 0)
        return '<';
    return 'B';
}

void decodeDirection(int number, int *dx, int *dy)
{
    switch (number)
    {
        break; case 0:
            *dx = 0;
            *dy = 1;
        break; case 1:
            *dx = 1;
            *dy = 0;
        break; case 2:
            *dx = 0;
            *dy = -1;
        break; case 3:
            *dx = -1;
            *dy = 0;
        break;
    }
}

void turn(int id, int numberOfDirection)
{
    decodeDirection((numberOfDirection + 
        getNumberByDirection(getDirection(clients[id].dx[LAST], clients[id].dy[LAST]))) % DIRECTIONS,
        &clients[id].dx[LAST],
        &clients[id].dy[LAST]);
}

int getNumberByDirection(char direction)
{
    switch(direction)
    {
        case '^':
            return 0;
        case '>':
            return 1;
        case 'v':
            return 2;
        case '<':
            return 3;
    }
    return 4;
}

void analyseShoot(int gameId, int id)
{
    printf("Analyzing\n");
    int x = clients[id].x[LAST];
    int y = clients[id].y[LAST];
    int dx = clients[id].dx[LAST];
    int dy = clients[id].dy[LAST];
    while (*accessToMap(&games[gameId], x + dx, y + dy) == '.')
    {
        x += dx;
        y += dy;
    }
    x += dx;
    y += dy;
    if (*accessToMap(&games[gameId], x, y) != '#')
    {
        for (int i = 0; i < games[gameId].numberOfClients; ++i)
        {
            printf("%d %d %d %d\n", clients[games[gameId].clients[i]].x[LAST], clients[games[gameId].clients[i]].y[LAST], x, y);
            if (clients[games[gameId].clients[i]].x[LAST] == x && 
                clients[games[gameId].clients[i]].y[LAST] == y)
            {
                printf("%d\n", i);
                clients[games[gameId].clients[i]].mode = CLIENT_MODE_SPECTATOR;
                *accessToMap(&games[gameId], x, y) = '.';
                break;
            }
        }
    }
}

void shiftLag(int gameId)
{
    for (int j = 0; j < games[gameId].numberOfClients; ++j)
    {
        int id = games[gameId].clients[j];
        for (int i = 0; i < LAST; ++i)
        {
            clients[id].x[i] = clients[id].x[i + 1];
            clients[id].y[i] = clients[id].y[i + 1];
            clients[id].dx[i] = clients[id].dx[i + 1];
            clients[id].dy[i] = clients[id].dy[i + 1];
        }
    }
    printf("Shifted\n");
}

void analyseMove(int gameId, int socket)
{
    int id = findClientBySocket(socket);
    printf("mode: %d\n", clients[id].mode);
    if (clients[id].mode != CLIENT_MODE_GAMER)
    {
        return;
    }
    int length, move;
    receivePackageStyle(socket, &length, &move, NULL);
    printf("move: %d\n", move);
    int dx;
    int dy;
    
    switch (move)
    {
        case SEND_MOVE_UP:
            clients[id].dx[LAST] = 0;
            clients[id].dy[LAST] = 1;
        break; case SEND_MOVE_RIGHT:
            clients[id].dx[LAST] = 1;
            clients[id].dy[LAST] = 0;
        break; case SEND_MOVE_DOWN:
            clients[id].dx[LAST] = 0;
            clients[id].dy[LAST] = -1;
        break; case SEND_MOVE_LEFT:
            clients[id].dx[LAST] = -1;
            clients[id].dy[LAST] = 0;
        break; case SEND_MOVE_FORWARD:
            *accessToMap(&games[gameId], clients[id].x[LAST], clients[id].y[LAST]) = '.';
//             if (!getNumberByDirection(getDirection(dx, dy)))
//             {
                if (*accessToMap(&games[gameId], clients[id].x[LAST] + clients[id].dx[LAST], clients[id].y[LAST] + clients[id].dy[LAST]) == '.')
                {
                    clients[id].x[LAST] += clients[id].dx[LAST];
                    clients[id].y[LAST] += clients[id].dy[LAST];
                }
//             }
        break; case SEND_MOVE_SHOOT:
            analyseShoot(gameId, id);
            return;
        break;
    }
    //turn(id, getNumberByDirection(getDirection(dx, dy)));
    //printf("oldCoords: %d %d\n", clients[id].x, clients[id].y);
    
    *accessToMap(&games[gameId], clients[id].x[LAST], clients[id].y[LAST]) = getDirection(clients[id].dx[LAST], clients[id].dy[LAST]);
//     printf("newCoords: %d %d\n", clients[id].x, clients[id].y);
}

void sendMap(int gameId, int socket)
{
    int id = findClientBySocket(socket);
    char *package = (char *)malloc(2 * sizeof(int) + games[gameId].W * games[gameId].H * sizeof(char));
    checkError("malloc");
    ((int *)package)[0] = games[gameId].W;
    ((int *)package)[1] = games[gameId].H;
    
    if (clients[id].mode == CLIENT_MODE_SPECTATOR)
    {
        memcpy(package + 2 * sizeof(int), games[gameId].map, games[gameId].W * games[gameId].H);
    }
    else
    {
        for (int x = 0; x < games[gameId].W; ++x)
        {
            for (int y = 0; y < games[gameId].H; ++y)
            {
                if (visibleDirected(gameId, id, x, y) && visibleBlocksWeak(gameId, x, y, clients[id].x[LAST], clients[id].y[LAST]))
                {
                    *(package + 2 * sizeof(int) + games[gameId].H * x + y) = *accessToMap(&games[gameId], x, y);
                }
                else
                {
                    *(package + 2 * sizeof(int) + games[gameId].H * x + y) = ' ';
                }
            }
        }
        
        for (int i = 0; i < games[gameId].numberOfClients; ++i)
        {
            int curId = games[gameId].clients[i];
            if (curId != id)
            {
                int x1 = clients[curId].x[LAST];
                int y1 = clients[curId].y[LAST];
                if (*(package + 2 * sizeof(int) + games[gameId].H * x1 + y1) != ' ')
                {
                    *(package + 2 * sizeof(int) + games[gameId].H * x1 + y1) = '.';
                }
                
            }
        }
        
        for (int i = 0; i < games[gameId].numberOfClients; ++i)
        {
            int curId = games[gameId].clients[i];
            if (curId != id)
            {
                int x0 = clients[curId].x[0];
                int y0 = clients[curId].y[0];
                if (*(package + 2 * sizeof(int) + games[gameId].H * x0 + y0) != ' ')
                {
                    *(package + 2 * sizeof(int) + games[gameId].H * x0 + y0) = getDirection(clients[curId].dx[0], clients[curId].dy[0]);
                }
            }
        }
    }
    sendPackageStyle(socket, 2 * sizeof(int) + games[gameId].W * games[gameId].H, SEND_MAP, package);
    free(package);
}

void *gameProcessor(void *args)
{
    int gameId = *(int *)(args);
    
    char pathToLog[MAX_NAME_SIZE];
    sprintf(pathToLog, "%s%d.txt", logDir, gameNumber++);
    FILE *currentLog = fopen(pathToLog, "w");
    fwrite(&games[gameId].W, sizeof(char), sizeof(int), currentLog);
    fwrite(&games[gameId].H, sizeof(char), sizeof(int), currentLog);
    
    gameEpollInit(gameId);
    printf("epoll inited\n");
    while (true)
    {
        int alive = 0;
        shiftLag(gameId);
        fwrite(games[gameId].map, sizeof(char), games[gameId].H * games[gameId].W, currentLog);
        for (int i = 0; i < games[gameId].numberOfClients; ++i)
        {
            sendMap(gameId, clients[games[gameId].clients[i]].socket);
            if (clients[games[gameId].clients[i]].mode == CLIENT_MODE_GAMER)
                ++alive;
        }
        if (alive == 1)
        {
            usleep(200000);
            deleteGame(gameId);
            fclose(currentLog);
            return NULL;
        }
        printf("tread epoll started waiting\n");
        int ndfs = epoll_pwait(games[gameId].epfd, games[gameId].events, MAX_CLIENTS_IN_GAME, epollTimeOut, NULL);
        printf("tread epoll ended waiting\n");
        
        for (int i = 0; i < ndfs; ++i)
        {
            if (!((~games[gameId].events[i].events) & EPOLLRDHUP))
            {
                deleteClient(findClientBySocket(games[gameId].events[i].data.fd));
                printf("deleted %d\n", games[gameId].events[i].data.fd);
                continue;
            }
            analyseMove(gameId, games[gameId].events[i].data.fd);
        }
    }
    fclose(currentLog);
    return NULL;
}
#ifndef _SERVER_HEADER
#define _SERVER_HEADER

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <errno.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>

#define MAXW 256
#define MAX_GAMES 128
#define MAX_CLIENTS_IN_GAME 10
#define MAX_CLIENTS 128
#define MAX_SUM_MAPS_NAMES_LEN 1024
#define MAX_NAME_SIZE 20
#define MAX_MESSAGE_LENGTH 20
#define MAX_ALARM_LENGTH 30
#define DIRECTIONS 4
#define LAG 5
#define LAST LAG - 1

#define logFile stdout

#include "sendDefines.h"

#define GAME_HOST_ID 0

#define ALARM_CONNECTED 0
#define ALARM_DISCONNECTED 1

typedef struct
{  
    char *name;
    int game;
    
    int x[LAG];
    int y[LAG];
    int dx[LAG];
    int dy[LAG];
    
    int socket;
    int mode;
}
Client;

typedef struct
{
    char *map;
    int W;
    int H;
    int R;
    bool started;
    int clients[MAX_CLIENTS_IN_GAME];
    int numberOfClients;
    
    int epfd; //is created at start;
    struct epoll_event ev;
    struct epoll_event events[MAX_CLIENTS_IN_GAME];
    
    pthread_t thread;
}
Game;

typedef struct
{
    pthread_t thread;
    int args[3];
}
Viewer;


void handleError(char *message);
void checkError(char *message);
void checkBrokenPipe(int socket);
void die(int signal);
void daemonize(int argc, char **argv);
char *accessToMap(Game *game, int x, int y);
void loadListOfMaps(void);
void registerHandler(int signal, void (*handler)(int));
void init(int argc, char **argv);
int addClient(int socket, char *name);
void swap(void *first, void *second, int size);
void deleteClient(int id);
int findClientBySocket(int socket);
void loadMap(Game *game, char *mapName);

void addClientToGame(int id, int gameId);
void makeGamer(int id, int gameId);
void makeSpectator(int id, int gameId);

int addGame(int creator, char *map);
void deleteGame(int id);
int getViewer(void);
void releaseViewer(int id);
bool sendAll(int socket, void *buffer, int length);
bool sendPackageStyle(int socket, int length, int additionalInfo, void *buffer);
bool receiveAll(int socket, void *buffer, int length);
bool receivePackageStyle(int socket, int *length, int *additionalInfo, void *buffer);
void alarmAll(int mode, int id, int status);
void sendListOfMaps(int socket);
void sendListOfActiveGames(int socket);
void sendIntroductionMessage(int socket);
void sendListOfPlayers(int socket, int gameId);
void createNewGame(int socket, char *mapFile);
void connectToGame(int socket, int gameId, int mode);
void tryStartGame(int socket);
void setupConnection(char *host, char *port);
void mainEpollInit(void);
void mainEpollProcess(void);
void clientProcessor(int socket);
int main(int argc, char **argv);

//serverGameProcessor.c
int max(int a, int b);
int min(int a, int b);
bool belongs(int A, int B, int C);
bool onLineIntersection(int y1, int y2, int y3, int y4);
int sign(int x);
bool vectorCheck(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
bool visibleBlocks(int gameId, int x, int y, int x0, int y0);
bool intersectsSegments(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
bool intersects(int x1, int y1, int x2, int y2, int x3, int y3);
void gameEpollInit(int gameId);
int sq(int x);
bool visibleUndirected(int gameId, int id, int x, int y);
bool visibleDirected(int gameId, int id, int x, int y);
char getDirection(int dx, int dy);
void decodeDirection(int number, int *dx, int *dy);
void turn(int id, int numberOfDirection);
int getNumberByDirection(char direction);
void analyseShoot(int gameId, int id);
void shiftLag(int gameId);
void analyseMove(int gameId, int socket);
void gameEpollInit(int gameId);
void *gameProcessor(void *args);


void sendFullMap(int W, int H, char *map, int socket);
void *startSendingLog(void *args);

#endif
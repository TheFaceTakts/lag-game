#include "serverHeader.h"

const char MAPS_PATH[] = "maps";
const char STARTED[] = "started";
const char NOT_STARTED[] = "not started";


int mainSocket;
struct epoll_event mainEv, mainEvents[MAX_CLIENTS];
int mainEpollFileDescriptor;
const int epollTimeOut = 500;
int gameNumber = 0;
const char logDir[] = "log/";

char introductionMessage[] = "\
\033c\
Welcome!\n\
Available commands:\n\
list - list of current games \n\
maps - list of available maps \n\
new [map] - create new game \n\
connect [gameNumber] [mode] - connect to existing game \n\
players [gameNumber] - list of players in a game \n\
start - start a game if you created one \n\
";

Game games[MAX_GAMES];
int freeGameIds[MAX_GAMES];
int lastFreeGame;

Client clients[MAX_CLIENTS];
int freeClientIds[MAX_CLIENTS];
int lastFreeClient;

Viewer viewers[MAX_CLIENTS];
int freeViewersIds[MAX_CLIENTS];
int lastFreeViewer;

char listOfMaps[MAX_SUM_MAPS_NAMES_LEN];
int listOfMapsLength = 0;

const char *const gLockFilePath = "/var/run/lagGameServer.pid";
FILE *LOG_FILE;


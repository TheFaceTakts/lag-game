#include "externs.h"


void handleError(char *message)
{
    perror(message);
    exit(1);
}

void checkError(char *message)
{
    if (errno)
        handleError(message);
}

void checkBrokenPipe(int socket)
{
    if (errno == ECONNRESET || errno == EPIPE)
    {
        errno = 0;
        //deleteClient(findClientBySocket(socket));
    }
}

void die(int signal)
{
    fclose(LOG_FILE);
    close(mainSocket);
    close(mainEpollFileDescriptor);
    exit(0);
}


void daemonize(int argc, char **argv)
{
    int pid = fork();
    checkError("fork");
    if (!pid)
    {
        umask(0);
        setsid();
        
        dup2(STDERR_FILENO, STDOUT_FILENO);
        
        
        LOG_FILE = freopen("errorLog.txt", "w", stdout);
        checkError("freopen");
        printf("GUT\n");
        FILE *pidFile = fopen(gLockFilePath, "w");
        checkError("fopen");
        
        fprintf(pidFile, "%d\n", getpid());
        fclose(pidFile);
        
        init(argc, argv);
    }
}

char *accessToMap(Game *game, int x, int y)
{
    return (game->map + game->H * x + y);
}

void loadListOfMaps()
{
    DIR *mapsDirectory = opendir(MAPS_PATH);
    if (mapsDirectory == NULL)
        handleError("opendir");
    struct dirent *entry;
    while ( (entry = readdir(mapsDirectory) ) )
    {
        if (entry->d_name[0] != '.')
        {
            int lengthAddition = 0;
            sprintf(listOfMaps + listOfMapsLength, "%s\n%n", entry->d_name, &lengthAddition);
            listOfMapsLength += lengthAddition;
        }
    }
    if (errno)
        handleError("readdir");
    closedir(mapsDirectory);
    listOfMaps[listOfMapsLength] = '\0';
}

void registerHandler(int signal, void (*handler)(int))
{
    struct sigaction action;
    action.sa_handler = handler;
    sigemptyset(&action.sa_mask);
    checkError("sigemptyset");
    action.sa_flags = 0;
    sigaction (signal, &action, NULL);
    checkError("sigaction");
}

void init(int argc, char **argv)
{
    for (int i = 0; i < MAX_GAMES; ++i)
    {
        freeGameIds[i] = i;
        games[i].numberOfClients = 0;
    }
    
    lastFreeGame = MAX_GAMES - 1;
    
    for (int i = 0; i < MAX_CLIENTS; ++i)
    {
        freeClientIds[i] = i;
        clients[i].game = CLIENT_NO_GAME_SELECTED;
        clients[i].mode = CLIENT_MODE_DISCONNECTED;
    }
    
    lastFreeClient = MAX_CLIENTS - 1;
    loadListOfMaps();
    registerHandler(SIGPIPE, SIG_IGN);
    registerHandler(SIGTERM, die);
    
    setupConnection(argv[1], argv[2]);
    mainEpollInit();
    mainEpollProcess();
}

int addClient(int socket, char *name)
{
    int id = freeClientIds[lastFreeClient--];
    
    clients[id].socket = socket;
    clients[id].name = name;
    clients[id].mode = CLIENT_MODE_UNDEFINED;
    clients[id].game = CLIENT_NO_GAME_SELECTED;
    return id;
}

void swap(void *first, void *second, int size)
{
    char temp[size];
    memcpy(temp, first, size);
    memcpy(first, second, size);
    memcpy(second, temp, size);
}

void deleteClient(int id)
{
    printf("trying to delete %d\n", id);
    if (id == -1)
        return;
    if (clients[id].game != CLIENT_NO_GAME_SELECTED)
    {
        for (int i = 0; i < games[clients[id].game].numberOfClients; ++i)
        {
            if (games[clients[id].game].clients[i] == id)
            {
                swap(&games[clients[id].game].clients[i], 
                     &games[clients[id].game].clients[games[clients[id].game].numberOfClients - 1],
                     sizeof(int));
                --games[clients[id].game].numberOfClients;
                break;
            }
        }
        if (games[clients[id].game].started)
        {
            printf("cleaning\n");
            *accessToMap(&games[clients[id].game], clients[id].x[LAST], clients[id].y[LAST]) = '.';
            epoll_ctl(games[clients[id].game].epfd, EPOLL_CTL_DEL, clients[id].socket, NULL);
            checkBrokenPipe(clients[id].socket);
            checkError("epoll_ctl delete Client");
        }
    }
    else if (clients[id].mode != CLIENT_MODE_WATCHING_LOG)
    {
        
        epoll_ctl(mainEpollFileDescriptor, EPOLL_CTL_DEL, clients[id].socket, NULL);
        checkBrokenPipe(clients[id].socket);
        checkError("epoll_ctl");
    }
    close(clients[id].socket);
    clients[id].game = CLIENT_NO_GAME_SELECTED;
    clients[id].mode = CLIENT_MODE_DISCONNECTED;
    alarmAll(CLIENT_MODE_UNDEFINED, id, ALARM_DISCONNECTED);
    free(clients[id].name);
    freeClientIds[++lastFreeClient] = id;
    
    
}

int findClientBySocket(int socket)
{
    for (int i = 0; i < MAX_CLIENTS; ++i)
    {
        if (clients[i].socket == socket)
        {
            return i;
        }
    }
    return -1;
}

void loadMap(Game *game, char *mapName)
 {
    int mapsPathNameLength = strlen(MAPS_PATH);
    int mapNameLength = strlen(mapName);
    char *newName = (char *)malloc(mapNameLength + mapsPathNameLength + 1 + 1);
    memcpy(newName, MAPS_PATH, mapsPathNameLength);
    newName[mapsPathNameLength] = '/';
    
    memcpy(newName + mapsPathNameLength + 1, mapName, mapNameLength);
    newName[mapNameLength + mapsPathNameLength + 1] = '\0';
    
    FILE *file = fopen(newName, "r");
    checkError("fopen");
    
    free(newName);
    
    
    fscanf(file, "%d %d", &game->H, &game->W);
    checkError("fscanf");
    
    game->map = (char *)malloc(game->H * game->W * sizeof(char));
    checkError("malloc");
    
    for (int i = 0; i < game->H; ++i)
    {
        for (int j = 0; j < game->W; ++j)
        {
            fscanf(file, " %c", accessToMap(game, j, game->H - i - 1));
            checkError("fscanf");
        }
    }
    fclose(file);
//     for (int j = game->H - 1; j >= 0; --j)
//     {
//         for (int i = 0; i < game->W; ++i)
//         {
//             printf("%c", *accessToMap(game, i, j));
//         }
//         printf("\n");
//     }
}


void addClientToGame(int id, int gameId)
{
    games[gameId].clients[games[gameId].numberOfClients] = id;
    ++games[gameId].numberOfClients;
    clients[id].game = gameId;
}

void makeGamer(int id, int gameId)
{
    addClientToGame(id, gameId);
    clients[id].mode = CLIENT_MODE_GAMER;
    clients[id].dx[0] = 0;
    clients[id].dy[0] = 1;
    
    do
    {
        clients[id].x[0] = rand() % games[gameId].W;
        clients[id].y[0] = rand() % games[gameId].H;
    }
    while (*accessToMap(&games[gameId], clients[id].x[0], clients[id].y[0]) != '.');
    for (int i = 1; i < LAG; ++i)
    {
        clients[id].x[i] = clients[id].x[0];
        clients[id].y[i] = clients[id].y[0];
        clients[id].dx[i] = clients[id].dx[0];
        clients[id].dy[i] = clients[id].dy[0];
    }
    *accessToMap(&games[gameId], clients[id].x[LAST], clients[id].y[LAST]) = '^';
}

void makeSpectator(int id, int gameId)
{
    addClientToGame(id, gameId);
    clients[id].mode = CLIENT_MODE_SPECTATOR;
}

int addGame(int creator, char *map)
{
    int id = freeGameIds[lastFreeGame--];
    fprintf(logFile, "%d %d\n", id, lastFreeGame);
    loadMap(&games[id], map);
    games[id].R = 10;
    games[id].started = false;
    fprintf(logFile, "%d %d\n", id, games[id].numberOfClients);
    makeGamer(creator, id);
    return id;
}

void deleteGame(int id)
{
    for (int i = 0; i < games[id].numberOfClients; ++i)
    {
        clients[games[id].clients[i]].mode = CLIENT_MODE_UNDEFINED;
        clients[games[id].clients[i]].game = CLIENT_NO_GAME_SELECTED;
        if (games[id].started)
        {
            epoll_ctl(games[id].epfd, EPOLL_CTL_DEL, clients[games[id].clients[i]].socket, NULL);
            checkBrokenPipe(clients[games[id].clients[i]].socket);
            checkError("epoll_ctl");
            
            mainEv.events = EPOLLIN | EPOLLET | EPOLLRDHUP;
            mainEv.data.fd = clients[games[id].clients[i]].socket;
            epoll_ctl(mainEpollFileDescriptor, EPOLL_CTL_ADD, clients[games[id].clients[i]].socket, &mainEv);
            checkBrokenPipe(clients[games[id].clients[i]].socket);
            checkError("epoll_ctl");
            sendIntroductionMessage(clients[games[id].clients[i]].socket);
        }
    }
    games[id].numberOfClients = 0;
    if (games[id].started)
    {
        close(games[id].epfd);
        games[id].started = false;
    }
    free(games[id].map);
    freeGameIds[++lastFreeGame] = id;
}

int getViewer()
{
    return freeViewersIds[lastFreeViewer--];
}

void releaseViewer(int id)
{
    freeViewersIds[++lastFreeViewer] = id;
}

bool sendAll(int socket, void *buffer, int length)
{
    if (length == 0)
        return true;
    int total = 0;
    int current = 0;
    while (total < length && (current = write(socket, buffer + total, length - total) ) )
    {
        if (current == -1)
        {
            if (errno == ECONNRESET || errno == EPIPE)
            {
                deleteClient(findClientBySocket(socket));
                errno = 0;
                return false;
            }
            handleError("write");
        }
        total += current;
    }
    if (current == 0) //means client has disconnected;
    {
        deleteClient(findClientBySocket(socket));
        return false;
    }
    return true;
}

bool sendPackageStyle(int socket, int length, int additionalInfo, void *buffer)
{
    if (sendAll(socket, &length, sizeof(int)))
        if (sendAll(socket, &additionalInfo, sizeof(int)))
            if (sendAll(socket, buffer, length))
                return true;
    return false;
}

bool receiveAll(int socket, void *buffer, int length)
{
    if (length == 0)
        return true;
    int current = 0;
    int total = 0;
    while (total < length && (current = read(socket, buffer + total, length - total)))
    {
        if (errno == ECONNRESET)
        {
            deleteClient(findClientBySocket(socket));
            errno = 0;
            return false;
        }
        checkError("current");
        total += current;
    }
    if (current == 0)
    {
        deleteClient(findClientBySocket(socket));
        return false;
    }
    return true;
}

bool receivePackageStyle(int socket, int *length, int *additionalInfo, void *buffer)
{
    if (receiveAll(socket, length, sizeof(int)))
        if (receiveAll(socket, additionalInfo, sizeof(int)))
            if (receiveAll(socket, buffer, *length))
                return true;
    return false;
}


void alarmAll(int mode, int id, int status)
{
    char *name = clients[id].name;
    char *result = (char *)malloc(strlen(name) + MAX_ALARM_LENGTH);
    int length;
    if (status == ALARM_CONNECTED)
        sprintf(result, "%s connected\n%n", name, &length);
    else if (status == ALARM_DISCONNECTED)
        sprintf(result, "%s disconnected\n%n", name, &length);
    result[length] = '\0';
    for (int i = 0; i < MAX_CLIENTS; ++i)
        if (clients[i].mode == mode && i != id)
        {
            printf("alarm %d\n", i);
            sendPackageStyle(clients[i].socket, length + 1, SEND_ALARM, result);
            printf("alarm sent\n");
        }
    free(result);
}

void sendListOfMaps(int socket)
{
    sendPackageStyle(socket, listOfMapsLength + 1, SEND_LIST_OF_MAPS, listOfMaps);
}

void sendListOfActiveGames(int socket)
{
    int total = 0;
    int current = 0;
    char *listOfActiveGames = (char *)malloc((3 + strlen(NOT_STARTED) + 2) * (MAX_GAMES - lastFreeGame + 1) + 1);
                                    //max number length               space + \n
    checkError("malloc");
    
    int lenListOfActiveGames = 0;
    int lengthAddition = 0;
    for (int i = 0; i < MAX_GAMES; ++i)
    {
        if (games[i].numberOfClients)
        {
            sprintf(listOfActiveGames + lenListOfActiveGames, "%d %s\n%n", i, (games[i].started ? STARTED : NOT_STARTED), &lengthAddition);
            lenListOfActiveGames += lengthAddition;
        }
    }
    listOfActiveGames[lenListOfActiveGames] = '\0';
    printf("%s %d\n", listOfActiveGames, lenListOfActiveGames);
    sendPackageStyle(socket, lenListOfActiveGames + 1, SEND_LIST_OF_GAMES, listOfActiveGames);
    free(listOfActiveGames);
}

void sendIntroductionMessage(int socket)
{
    sendPackageStyle(socket, strlen(introductionMessage) + 1, SEND_DESCRIPTION, introductionMessage);
}

void sendListOfPlayers(int socket, int gameId)
{
    char *message = (char *)malloc((games[gameId].numberOfClients * (MAX_NAME_SIZE + strlen("spectator") + 3) + 1) * sizeof(char));
    int currentLength = 0;
    int lengthAddition = 0;
    for (int i = 0; i < games[gameId].numberOfClients; ++i)
    {
        sprintf(message + currentLength, "%s %s\n%n", clients[games[gameId].clients[i]].name, 
                clients[games[gameId].clients[i]].mode == CLIENT_MODE_SPECTATOR ? "spectator" : "gamer", 
                &lengthAddition);
        currentLength += lengthAddition;
    }
    message[currentLength] = '\0';
    printf("1\n");
    sendPackageStyle(socket, currentLength + 1, SEND_LIST_OF_PLAYERS_IN_GAME, message);
    free(message);
}

void createNewGame(int socket, char *mapFile)
{
    int id = findClientBySocket(socket);
    if (id == -1)
        return;
    if (clients[id].game != CLIENT_NO_GAME_SELECTED)
    {
        sendPackageStyle(socket, strlen("You are already in a game\n"), SEND_ALREADY_IN_GAME, "You are already in a game\n");
        return;
    }
    fprintf(logFile, "%s\n", mapFile);
    addGame(id, mapFile);
}

void connectToGame(int socket, int gameId, int mode)
{
    int id = findClientBySocket(socket);
    if (id == -1)
        return;
    if (clients[id].game != CLIENT_NO_GAME_SELECTED)
    {
        sendPackageStyle(socket, strlen("You are already in a game\n"), SEND_ALREADY_IN_GAME, "You are already in a game\n");
        return;
    }
    if (games[gameId].numberOfClients == MAX_CLIENTS_IN_GAME)
    {
        sendPackageStyle(socket, strlen("Too many clients in game\n"), SEND_GAME_OVERFLOW, "Too many clients in game\n");
        return;
    }
    
    if (mode == CLIENT_MODE_SPECTATOR)
        makeSpectator(id, gameId);
    else if (mode == CLIENT_MODE_GAMER)
        makeGamer(id, gameId);
}

void tryStartGame(int socket)
{
    int id = findClientBySocket(socket);
    if (id == -1)
        return;
    if (clients[id].game == CLIENT_NO_GAME_SELECTED ||
        games[clients[id].game].clients[0] != id)
    {
        sendPackageStyle(socket, strlen("You are not allowed to start a game\n") + 1, SEND_NOT_ALLOWED, "You are not allowed to start a game\n");
        return;
    }
    games[clients[id].game].started = true;
    int *gameIdPtr = &clients[id].game;
    pthread_create(&games[*gameIdPtr].thread, NULL, gameProcessor, gameIdPtr);
}

void setupConnection(char *host, char *port)    
{
    mainSocket = socket(AF_INET, SOCK_STREAM, 0);
    checkError("socket");
    
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(port));
    addr.sin_addr.s_addr = inet_addr(host);
    
    const int optVal = 1;
    const socklen_t optLen = sizeof(optVal);

    setsockopt(mainSocket, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen);
    checkError("setsockopt");
    
    bind(mainSocket, (struct sockaddr *)&addr, sizeof(addr));
    checkError("bind");
    
    listen(mainSocket, MAX_CLIENTS);
    checkError("listen");
}

void mainEpollInit()
{
    mainEpollFileDescriptor = epoll_create(MAX_CLIENTS);
    checkError("epoll_create");
    
    mainEv.events = EPOLLIN;
    mainEv.data.fd = mainSocket;
    
    epoll_ctl(mainEpollFileDescriptor, EPOLL_CTL_ADD, mainSocket, &mainEv);
    checkError("epoll_ctl");
}

void mainEpollProcess()
{
    int cnt = 0;
    while (1)
    {
        fprintf(logFile, "main epoll started waiting %d\n", cnt++);
        int ndfs = epoll_pwait(mainEpollFileDescriptor, mainEvents, MAX_CLIENTS, -1, NULL);
        checkError("epoll_pwait");
        fprintf(logFile, "main epoll ended waiting\n");
        
        for (int i = 0; i < ndfs; ++i)
        {
            if (mainEvents[i].data.fd == mainSocket)
            {
                printf("start waiting for accept\n");
                int socket = accept(mainSocket, NULL, NULL);
                printf("end waiting for accept\n");
                checkError("accept");
                
                mainEv.events = EPOLLIN | EPOLLET | EPOLLRDHUP;
                mainEv.data.fd = socket;
                
                epoll_ctl(mainEpollFileDescriptor, EPOLL_CTL_ADD, socket, &mainEv);
                checkBrokenPipe(socket);
                checkError("epoll_ctl");
                
                char *name = (char *)malloc(sizeof(char) * MAX_NAME_SIZE);
                checkError("malloc");
                int length, additionalInfo;
                
                receivePackageStyle(socket, &length, &additionalInfo, name);
                int id = addClient(socket, name);
                printf("%d %s\n", id, clients[id].name);
                sendIntroductionMessage(socket);
                if (clients[id].mode != CLIENT_MODE_DISCONNECTED)
                {
                    alarmAll(CLIENT_MODE_UNDEFINED, id, ALARM_CONNECTED);
                }
            }
            else
            {
                if (!((~mainEvents[i].events) & EPOLLRDHUP))
                {
                    deleteClient(findClientBySocket(mainEvents[i].data.fd));
                    printf("deleted %d\n", mainEvents[i].data.fd);
                    continue;
                }
                clientProcessor(mainEvents[i].data.fd);
            }
        }
    }
}

void clientProcessor(int socket)
{
    char buffer[MAX_MESSAGE_LENGTH];
    int length, additionalInfo;
    receivePackageStyle(socket, &length, &additionalInfo, buffer);
    if (additionalInfo == SEND_GET_LIST)
    {
        sendListOfActiveGames(socket);
    }
    else if (additionalInfo == SEND_GET_MAPS)
    {
        sendListOfMaps(socket);
    }
    else if (additionalInfo == SEND_NEW)
    {
        createNewGame(socket, buffer);
    }
    else if (additionalInfo == SEND_CONNECT)
    {
        connectToGame(socket, *(int *)buffer, *(int *)(buffer + sizeof(int)));
    }
    else if (additionalInfo == SEND_GET_PLAYERS)
    {
        sendListOfPlayers(socket, *(int *)buffer);
    }
    else if (additionalInfo == SEND_START)
    {
        tryStartGame(socket);
    }
    else if (additionalInfo == SEND_WATCH_LOG)
    {
        int id = getViewer();
        int args[3];
        viewers[id].args[0] = *(int *)buffer;
        viewers[id].args[1] = findClientBySocket(socket);
        viewers[id].args[2] = id;
        pthread_create(&viewers[id].thread, NULL, startSendingLog, viewers[id].args);
    }
    printf("OK %d %d\n", additionalInfo, socket);
}

int main(int argc, char **argv)
{
    daemonize(argc, argv);
    //init(argc, argv);
    return 0;
}